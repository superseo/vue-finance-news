import Vue from 'vue';
import BasePost from './posts/_base/_base';
import NewsPost from './posts/news/news';
import FinancePost from './posts/finance/finance';

let components = [
	{ name: 'c-post-base', component: BasePost },
	{ name: 'c-post-news', component: NewsPost },
	{ name: 'c-post-finance', component: FinancePost }
];

_.each(components, name => {
	if (_.isString(name)) {
		Vue.component(name, () => System.import(`./${name}.vue`).then(tpl => {
			tpl.name = name;
			return tpl;
		}, () => {
			location.reload();
		}));
	} else {
		Vue.component(name.name, name.component);
	}
});
