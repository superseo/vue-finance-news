import Vue from 'vue';
import Router from 'vue-router';

import Home from './pages/home';
import BasePost from './components/posts/_base/_base';

Vue.use(Router);

const routerConfig = {
	mode: 'history',
    routes: [
        {
            path: '/',
            component: Home,
            name: 'HomePage'
        },
        {
            path: '/post',
            component: BasePost,
            children: [
                {
                    path: '',
                    redirect: '/'
                },
                {
                    path: ':id',
                    component: BasePost,
                    name: 'ViewPostPage',
                    beforeEnter: (to, from, next) => {
                        to.params.viewItem = true;
                        next();
                    }
                }
            ]
        }
    ]
};

export default new Router(routerConfig);